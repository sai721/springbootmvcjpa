package com.sai;

import java.util.List;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
@Component
public class LoggingAspect {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);
	
	@Before("execution(public * com.sai.AlienController.getAliens())")
	public void log() {
		LOGGER.info("getaliens method called.......");
	}
	
	@After("execution(public * com.sai.AlienController.getAliens())")
	public void logAfter() {
		LOGGER.info("getaliens after called.......");
	}
}
