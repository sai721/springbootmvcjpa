package com.sai;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController
{
	@Autowired
	AlienRepo repo;
	
	@ModelAttribute
	public void modelData(Model m) {
		m.addAttribute("name","sai kumar");
	}
	
	@RequestMapping("/")
	public String home()
	{
		return "index";
	}
	
	
	@RequestMapping("add")
	public String add(@RequestParam("aid")int aid ,@RequestParam("name") String aname , ModelMap mv)
	{
		Alien alien=new Alien(aid,aname);
		
		mv.addAttribute("num3", alien);
		
		return  "result";
		
	}
	
	
	@PostMapping("addAlien")
	public String add(@ModelAttribute Alien a)
	{	
		repo.save(a);
		return  "result";
	}
	
	@RequestMapping("deleteAlien")
	public String deleteAlien(@ModelAttribute Alien a)
	{	
		repo.delete(a);
		return  "result";
	}
	
	@GetMapping("getAliens")
	public String getAliens(Model m) {
		
		m.addAttribute("result", repo.findAll());
		return "showAliens";
	}
	
	
	
	
	
	
	@RequestMapping("getAlien")
	public String getAlien(@RequestParam int aid ,  Model m) {
		
		m.addAttribute("result", repo.getOne(aid));
		return "showAliens";
	}
	
	
	@RequestMapping("getAlienByName")
	public String getAlienByName(@RequestParam("name") String name,  Model m) {
		
		m.addAttribute("result", repo.findByName(name));
		return "showAliens";
	}
	
	@RequestMapping("updateAlien")
	public String updateAlien(@RequestParam int aid ,@RequestParam("name") String aname ,  Model m) {
		
		Alien a=repo.findById(aid).get();
		a.name=aname;
		repo.save(a);
		m.addAttribute("result", "updated sucessfully");
		return "showAliens";
	}
	
	
}
