package com.sai;

import java.awt.PageAttributes.MediaType;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlienController {
	@Autowired
	AlienRepo repo;
	
	@GetMapping(path="aliens",produces= {"application/xml", "application/json"})
	public List getAliens() {
		
		List<Alien> aliens = repo.findAll();
		return aliens;
	}
	
	@PostMapping(path="alien", consumes= {"application/xml"})
	public Alien addAliens(@RequestBody Alien alien) {
		repo.save(alien);
		
		return alien;
	}
	
	//get alien by path
		@RequestMapping("/alien/{aid}")
		public Alien getAlienByPath(@PathVariable("aid") int aid) {
			
			Alien alien=repo.findById(aid).orElse(new Alien(0,""));
			return alien;
		}
}
