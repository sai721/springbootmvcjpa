<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<h3>get by id</h3>
	<form action="addAlien" method="POST">
 		Enter id : <input type="text" name="aid"><br>
 		Enter name : <input type="text" name="name"><br>
 		<input type="submit">
 	</form>
 	<hr/>
 	<h3>get by id</h3>
 	<form action="getAlien" method="GET">
 		Enter id : <input type="text" name="aid"><br>
 		<input type="submit">
 	</form>
 	
 	<hr/>
 	<h3>delete by id</h3>
 	<form action="deleteAlien" method="GET">
 		Enter id : <input type="text" name="aid"><br>
 		<input type="submit">
 	</form>
 	
 	<hr/>
 	<h3>update by id</h3>
 	<form action="updateAlien" method="POST">
 		Enter id : <input type="text" name="aid"><br>
 		Enter name : <input type="text" name="name"><br>
 		<input type="submit">
 	</form>
 	<hr/>
 	
 	
 	<hr/>
 	<h3>find by name</h3>
 	<form action="getAlienByName" method="GET">
 		Enter name : <input type="text" name="name"><br>
 		<input type="submit">
 	</form>
 	 
</body>
</html>